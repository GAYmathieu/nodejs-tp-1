const express = require('express')
const router = express.Router()
const commentaireController = require('../controller/commentaireController')
const middleware = require('../middleware/middlewareCorrAdmin')



router.get('/getCommentaire', middleware.authenticator, commentaireController.getAllCommentaire)
router.post('/addCommentaire', middleware.authenticator, commentaireController.addCommentaire)

module.exports = router;