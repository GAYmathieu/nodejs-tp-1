const express = require('express')
const router = express.Router()
const technoController = require('../controller/technoController')
const middleware = require('../middleware/middlewareCorrAdmin')



router.get('/getTechno', middleware.authenticator, technoController.getAllTechno)
router.post('/createTechno', middleware.authenticator, technoController.addTechno)
router.put('/editTechno/:id', middleware.authenticator, technoController.editTechno)
router.delete('/deleteTechno/:id', middleware.authenticator, technoController.deleteTechno)


module.exports = router;