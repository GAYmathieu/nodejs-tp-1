const jwt = require('jsonwebtoken')
require('dotenv').config()
const db = require('../database/database');

exports.authenticator = (req, res, next) => {
    // récupérer le token
    const authorization = req.headers.authorization;
    const token = authorization.split(' ')[1]
    console.log('Token:', token);
    if (token && process.env.SECRET_KEY) {
        jwt.verify(token, process.env.SECRET_KEY, async (err, decoded) => {
            // si problème => erreur
            if (err) {
                console.error(err);
                res.status(401).json({ erreur: "accès refusé" });
            }
            // décoder => next()
            else {
                try {
                    const result = await db.query('SELECT role FROM utilisateur WHERE email = ?', [decoded.email]);
                    console.log("result[0][0] : ", result[0][0].role);
                    if (result[0][0].role === 'administrateur' || result[0][0].role === 'journaliste') {
                        next();
                    } else {
                        res.status(403).json({ erreur: 'accès refusé, pas admin' });
                    }

                } catch (error) {
                    console.error(error);
                    res.status(500).json({ erreur: "Erreur serveur" });
                }
            }
        });
    } else {
        res.status(401).json({ erreur: "accès refusé" });
    }
};
