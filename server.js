const express = require('express');
const app = express();
const path = require('path');
const cors = require('cors');
const db = require('./database/database.js');
const bcrypt = require('bcrypt');

app.use(express.static(path.join(__dirname)));
app.use(express.json());
app.use(cors());

const userRoute = require('./route/userRoute.js')
app.use('/utilisateurs', userRoute)

const technoRoute = require('./route/technoRoute.js')
app.use('/techno', technoRoute)

const commentaireRoute = require('./route/commentaireRoute.js')
app.use('/commentaire', commentaireRoute)


app.use(express.static(path.join(__dirname, 'front')));

//index.html route
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'front', 'index.html'));
});




// obtenir la liste des utilisateurs
/*app.get('/utilisateurs', (req, res) => {
    const query = 'SELECT * FROM utilisateur';
  
    db.query(query, (err, result) => {
      if (err) {
        console.error('Erreur lors de la récupération des utilisateurs : ' + err.stack);
        res.status(500).json({ error: 'Erreur serveur' });
        return;
      }
  
      res.status(200).json(result);
    });
  });*/


// ajouter un utilisateur
app.post('/utilisateurs', async (req, res) => {
  const { nom, prenom, email, mdp } = req.body;

  try {
    const hashedPassword = await bcrypt.hash(mdp, 10);

    const query = 'INSERT INTO utilisateur (nom, prenom, email, mdp) VALUES (?, ?, ?, ?)';
    db.query(query, [nom, prenom, email, hashedPassword], (error) => {
      if (error) {
        console.error('Erreur lors de l\'ajout de l\'utilisateur : ' + error.stack);
        res.status(500).json({ error: 'Erreur serveur' });
        return;
      }

      res.status(200).json({ message: 'Utilisateur ajouté avec succès' });
    });
  } catch (error) {
    console.error('Erreur : ' + error.stack);
    res.status(500).json({ error: 'Erreur serveur' });
  }
});


// mettre à jour un utilisateur
app.put('/utilisateurs/:id', (req, res) => {
  const userId = req.params.id;
  const { nom, prenom, email, mdp } = req.body;
  const query = 'UPDATE utilisateur SET nom = ?, prenom = ?, email = ?, mdp = ? WHERE id = ?';

  db.query(query, [nom, prenom, email, mdp, userId], (result) => {
    res.status(200).json({ message: 'Utilisateur mis à jour avec succès', result });
  });
});


// supprimer un utilisateur
app.delete('/utilisateurs/:id', (req, res) => {
  const userId = req.params.id;
  const query = 'DELETE FROM utilisateur WHERE id = ?';

  db.query(query, [userId], (result) => {

    if (result.affectedRows === 0) {
      res.status(404).json({ error: 'Utilisateur non trouvé' });
      return;
    }

    res.status(200).json({ message: 'Utilisateur supprimé avec succès' });
  });
});


// obtenir la liste des commentaires
app.get('/commentaires', (req, res) => {
  const query = 'SELECT * FROM commentaire';

  db.query(query, (err, result) => {
    if (err) {
      console.error('Erreur lors de la récupération des commentaires : ' + err.stack);
      res.status(500).json({ error: 'Erreur serveur' });
      return;
    }

    res.status(200).json({ message: 'Liste des commentaires récupérée avec succès', commentaires: result });
  });
});


//obtenir commentaire d'un utilisateur spécific
app.get('/utilisateurs/:id/commentaires', (req, res) => {
  const userId = req.params.id;

  const getCommentsQuery = 'SELECT * FROM commentaire WHERE utilisateur_id = ?';
  db.query(getCommentsQuery, [userId], (err, result) => {
    if (err) {
      console.error('Erreur lors de la récupération des commentaires : ' + err.stack);
      res.status(500).json({ error: 'Erreur serveur' });
      return;
    }

    res.status(200).json({ message: 'Commentaires de l\'utilisateur récupérés avec succès', commentaires: result });
  });
});


// Obtenir commentaire avant 15/10/2023
app.get('/utilisateurs/commentairesDate', (req, res) => {

  const getCommentaire = 'SELECT * FROM commentaire WHERE date_creation_commentaire < "2023-12-15"';
  db.query(getCommentaire, (err, result) => {
    if (err) {
      console.error('Erreur lors de la récupération des commentaires : ' + err.stack);
      res.status(500).json({ error: 'Erreur serveur' });
      return;
    }

    res.status(200).json({ message: 'Commentaires antérieurs au 15/10/2023 récupérés avec succès', commentaires: result });
  });
});



app.listen(8000, function () {
  console.log("server open on port 8000");
})