const mysql = require('mysql2/promise')
require('dotenv').config()
const pool = mysql.createPool({
    host: process.env.DBHOST,
    user: process.env.DBUSER,
    database: process.env.DBDATABASE,
    port: process.env.DBPORT,
    password:process.env.DBPASSWORD,
  });

  module.exports = pool