//connexion
document.getElementById('loginForm').addEventListener('submit', async function (event) {
    event.preventDefault();

    const loginEmail = document.getElementById('loginEmail').value;
    const loginMdp = document.getElementById('loginMdp').value;

    try {
        const response = await fetch('http://localhost:8000/utilisateurs/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: loginEmail,
                mdp: loginMdp,
            }),
        });

        const data = await response.json();

        if (data.token) {
            localStorage.setItem('jwtToken', data.token);
            alert('vous êtes bien connecté')


            const resultContainer = document.getElementById('result');
            resultContainer.innerHTML = redirectButtons;
        } else {
            alert('Échec de la connexion');
        }
    } catch (error) {
        console.error('Erreur : ' + error.stack);
        alert('Erreur');
    }
});

function redirectToPage(page) {
    window.location.href = page;
}

//ajout d'une techno
document.getElementById('ajouterTechnoForm').addEventListener('submit', async function (event) {
    event.preventDefault();

    const nom_techno = document.getElementById('nom_techno').value;
    const date_creation = document.getElementById('date_creation').value;
    const nom_createur = document.getElementById('nom_createur').value;

    try {
        const response = await fetch('http://localhost:8000/techno/createTechno', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
            },
            body: JSON.stringify({
                nom_techno,
                date_creation,
                nom_createur,
            }),
        });

        const data = await response.json();
        alert(data.message);
    } catch (error) {
        console.error('Erreur : ' + error.stack);
        alert('Erreur');
    }
});


//ajout d'un commentaire
document.getElementById('addCommentaireForm').addEventListener('submit', async function (event) {
    event.preventDefault();

    const message = document.getElementById('message').value;

    try {
        const response = await fetch('http://localhost:8000/commentaire/addCommentaire', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
            },
            body: JSON.stringify({
                message
            }),
        });

        const data = await response.json();
        alert(data.message);
    } catch (error) {
        console.error('Erreur : ' + error.stack);
        alert('Erreur');
    }
});


//ajout d'un commentaire
document.getElementById('getCommentaire').addEventListener('click', async function () {
    try {
        const response = await fetch('http://localhost:8000/commentaire/getCommentaire', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
            },
        });

        const data = await response.json();
        
        displayComments(data);

    } catch (error) {
        console.error('Erreur : ' + error.stack);
        alert('Erreur, pas admin');
    }
});

function displayComments(comments) {
    const resultContainer = document.getElementById('resultContainer');
    resultContainer.innerHTML = '';

    comments.forEach(comment => {
        const commentElement = document.createElement('div');
        commentElement.textContent = comment.message;
        resultContainer.appendChild(commentElement);
    });
}

