const db = require("../database/database");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();


//liste des technos
exports.getAllTechno = async (req, res) => {
  try {
    const [rows, fields] = await db.query('SELECT * FROM technologie');
    res.status(200).json(rows);
  } catch (error) {
    console.error('Erreur lors de la récupération des techno : ' + error.stack);
    res.status(500).json({ error: 'Erreur serveur' });
  }
};

//ajout techno
exports.addTechno = async (req, res) => {
  const { nom_techno, date_creation, nom_createur } = req.body;

  try {
    const query = 'INSERT INTO technologie (nom_techno, date_creation, nom_createur) VALUES (?, ?, ?)';
    // Utilisez la promesse directement sans fonction de rappel
    await db.query(query, [nom_techno, date_creation, nom_createur]);

    res.status(200).json({ message: 'techno ajouté avec succès' });
  } catch (error) {
    console.error('Erreur lors de l\'ajout de la techno : ' + error.stack);
    res.status(500).json({ error: 'Erreur serveur' });
  }
};

//edit techno
exports.editTechno = async (req, res) => {
  try {
    const technoId = req.params.id;
    const { nom_techno, date_creation, nom_createur } = req.body;
    const query = 'UPDATE technologie SET nom_techno = ?, date_creation = ?, nom_createur = ? WHERE id = ?';

    // Utilisez la promesse directement sans fonction de rappel
    const result = await db.query(query, [nom_techno, date_creation, nom_createur, technoId]);

    if (result.affectedRows === 0) {
      res.status(404).json({ error: 'technologie non trouvée' });
      return;
    }

    res.status(200).json({ message: 'technologie mise à jour avec succès' });
  } catch (error) {
    console.error('Erreur lors de la mise à jour de la technologie :', error);
    res.status(500).json({ error: 'Erreur serveur' });
  }
};


//delete techno
exports.deleteTechno = async (req, res) => {
  const technoId = req.params.id;
  const query = 'DELETE FROM technologie WHERE id = ?';

  try {
    const result = await db.query(query, [technoId]);

    if (result.affectedRows === 0) {
      res.status(404).json({ error: 'technologie non trouvée' });
      return;
    }

    res.status(200).json({ message: 'technologie supprimée avec succès' });
  } catch (error) {
    console.error('Erreur lors de la suppression de la technologie : ' + error.stack);
    res.status(500).json({ error: 'Erreur serveur' });
  }
};
