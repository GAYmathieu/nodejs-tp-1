const db = require("../database/database");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();




//liste des commentaires
exports.getAllCommentaire = async (req, res) => {
    try {
        const [rows, fields] = await db.query('SELECT date_creation_commentaire, message FROM commentaire');
        res.status(200).json(rows);
    } catch (error) {
        console.error('Erreur lors de la récupération des commentaires : ' + error.stack);
        res.status(500).json({ error: 'Erreur serveur' });
    }
};



//ajout techno
exports.addCommentaire = async (req, res) => {
    const { message } = req.body;

    try {
        const query = 'INSERT INTO commentaire (message) VALUES (?)';
        // Utilisez la promesse directement sans fonction de rappel
        await db.query(query, [message]);

        res.status(200).json({ message: 'commentaire ajouté avec succès' });
    } catch (error) {
        console.error('Erreur lors de l\'ajout du commentaire : ' + error.stack);
        res.status(500).json({ error: 'Erreur serveur' });
    }
};